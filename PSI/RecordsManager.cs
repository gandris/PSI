﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PSI.Template;

namespace PSI
{
    public class RecordsManager
    {
        private readonly IReader<Record> _reader;

        public RecordsManager(IReader<Record> reader)
        {
            _reader = reader;
        }

        public void ListRecords()
        {
            var records = _reader.ReadAll();
            foreach (var record in records)
            {
                Console.WriteLine(record);
            }
        }
    }
}
