﻿using System;
using PSI.Template;

namespace PSI
{
    class Program
    {
        static void Main(string[] args)
        {
            var reader = new DatabaseReader<Record>(Config.DbName);
            var manager = new RecordsManager(reader);
            manager.ListRecords();
            Console.ReadKey();
        }
    }
}
