﻿using System.Collections.Generic;

namespace PSI
{
    public interface IReader<T>
    {
        T ReadSingle(int number);
        IEnumerable<T> ReadAll();
        string ReadAllAsString();
    }
}