﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace PSI.Template
{
    public class FileReader<T>:Reader<T>
    {
        private readonly string _fileName;
        private Stream _stream;

        public FileReader(string fileName)
        {
            _fileName = fileName;
        }

        protected override void Load()
        {
            _stream = File.Open(_fileName, FileMode.Open);
        }

        protected override void Dispose()
        {
            _stream.Close();
            _stream.Dispose();
        }

        protected override string Process()
        {
            return new StreamReader(_stream).ReadToEnd();
        }

        protected override IEnumerable<T> ProcessAll()
        {
            return JsonConvert.DeserializeObject<List<T>>(Process());
        }
    }
}
