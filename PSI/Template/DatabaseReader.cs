﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;

namespace PSI.Template
{
    public class DatabaseReader<T>: Reader<T>
    {
        private readonly string _databaseName;
        private MongoDatabase _db;
        private MongoServer _server;

        public DatabaseReader(string databaseName)
        {
            _databaseName = databaseName;
        }

        protected override void Load()
        {
            _server = new MongoServer(new MongoServerSettings());
            _server.Connect();
            _db = _server.GetDatabase(_databaseName);
        }

        protected override void Dispose()
        {
            _server.Disconnect();
        }

        protected override string Process()
        {
            var collection = _db.GetCollection<T>(typeof(T).Name);
            return collection.ToJson();
        }

        protected override IEnumerable<T> ProcessAll()
        {
            var c = _db.GetCollection<T>(typeof(T).Name);
            return c.FindAll().ToList();
        }
    }
}
