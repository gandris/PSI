﻿using System.Collections.Generic;
using System.Linq;

namespace PSI.Template
{
    public abstract class Reader<T>: IReader<T>
    {
        protected abstract void Load();
        protected abstract void Dispose();
        protected abstract string Process();
        protected abstract IEnumerable<T> ProcessAll();

        public T ReadSingle(int number)
        {
            try
            {
                Load();
                return ProcessAll().ElementAt(number);
            }
            finally
            {
                Dispose();
            }
        }

        public IEnumerable<T> ReadAll()
        {
            try
            {
                Load();
                return ProcessAll();
            }
            finally
            {
                Dispose();
            }
        }

        public string ReadAllAsString()
        {
            try
            {
                Load();
                return Process();
            }
            finally
            {
                Dispose();
            }
        }
    }
}
