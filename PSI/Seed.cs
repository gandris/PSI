﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace PSI
{
    public static class Seed
    {
        public static async Task Run()
        {
            var records = new List<Record>()
            {
                new Record()
                {
                    Content = "Hello world",
                    Title = "Hello"
                },
                new Record()
                {
                    Content = "Hi world",
                    Title = "hi"
                }
            };

            using (var stream = new StreamWriter(new FileStream(Config.File, FileMode.OpenOrCreate)))
            {
                var json = JsonConvert.SerializeObject(records);
                stream.Write(json);
            }

            var database = new MongoClient().GetDatabase(Config.DbName);
            await database.CreateCollectionAsync(typeof (Record).Name);
            await database.GetCollection<Record>(typeof (Record).Name).InsertManyAsync(records);
        }
    }
    public static class Config
    {
        public static string File { get { return "test.txt"; } }
        public static string DbName { get { return "local"; } }
    }
}
