﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace PSI.Strategy
{
    public class FileStorage<T>: IStorage<T>
    {
        private readonly string _fileName;

        public FileStorage(string fileName)
        {
            _fileName = fileName;
        }

        public IEnumerable<T> Read()
        {
            using (var sr = new StreamReader(new FileStream(_fileName, FileMode.Open)))
            {
                return JsonConvert.DeserializeObject<List<T>>(sr.ReadToEnd());
            }
        }
    }
}
