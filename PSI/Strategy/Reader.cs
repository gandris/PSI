﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using PSI.Template;

namespace PSI.Strategy
{
    public class Reader<T>: IReader<T>
    {
        private IStorage<T> _storage;

        public Reader(IStorage<T> storage)
        {
            _storage = storage;
        }

        public T ReadSingle(int number)
        {
            return _storage.Read().ElementAt(0);
        }

        public IEnumerable<T> ReadAll()
        {
            return _storage.Read();
        }

        public string ReadAllAsString()
        {
            return JsonConvert.SerializeObject(_storage.Read());
        }
    }
}
