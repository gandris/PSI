﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;

namespace PSI.Strategy
{
    public class DatabaseStorage<T>: IStorage<T>
    {
        private readonly MongoDatabase _db;

        public DatabaseStorage(string databaseName)
        {
            var server = new MongoServer(new MongoServerSettings());
            server.Connect();
            _db = server.GetDatabase(databaseName);
        }

        public IEnumerable<T> Read()
        {
            return _db.GetCollection<T>(typeof (T).Name).FindAll().ToList();
        }
    }
}
