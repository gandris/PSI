﻿using System.Collections.Generic;

namespace PSI.Strategy
{
    public interface IStorage<T>
    {
        IEnumerable<T> Read();
    }
}
