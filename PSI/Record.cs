﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace PSI
{
    public class Record
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public override string ToString()
        {
            return Title + ": " + Content;
        }
    }
}
